set -e

./gradlew clean build
docker build -t dwg-test .
docker run -i -v $(pwd)/lew:/lew dwg-test