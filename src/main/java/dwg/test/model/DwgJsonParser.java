package dwg.test.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import dwg.test.DrawLines;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

public class DwgJsonParser {


    public static void main(String args[]) throws Exception {

        DwgJsonParser parser = new DwgJsonParser(new ObjectMapper());
        File file = new File("/Users/lewisfoster/dev/DWGtest/lew/output.json");
        DwgResult dwgResult = parser.parse(new String(Files.readAllBytes(file.toPath())));

        DrawLines.drawLines(dwgResult);

    }

    private final ObjectMapper mapper;

    public DwgJsonParser(ObjectMapper objectMapper) {
        this.mapper = objectMapper;
    }

    DwgResult parse(String json) throws Exception {
        List<Map<String, ?>> objects = (List<Map<String, ?>>) mapper.readValue(json, Map.class).get("OBJECTS");

        Map<String, Map<String, ?>> entities = objects.stream().filter(o -> o.containsKey("entity"))
                .collect(toMap(e -> Handle.toHandle(e.get("handle")), Function.identity()));


        List<Block> blocks = objects
                .stream()
                .filter(o -> "BLOCK_HEADER".equals(o.get("object")))
                .map(b -> toBlock(b, entities))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());

        List<Entity> allEntities = objects.stream()
                .filter(Entity::isSupported)
                .map(Entity::fromMap)
                .collect(Collectors.toList());

        Map<String, Layer> layers = layers(objects);

        Map<Layer, List<Line>> lineMap = new HashMap<>();

        allEntities.forEach(entity -> {
            safeAdd(lineMap, layers.get(entity.getLayerHandle()), entity.asLines());
        });

        for (Block block : blocks) {
            for (Insertion insertion : block.insertions()) {
                for (Entity entity : block.entities()) {

                    safeAdd(lineMap, layers.get(entity.getLayerHandle()),
                            entity.asLines()
                                    .stream()
                                    .map(line -> line.relativeTo(insertion.point()))
                                    .collect(Collectors.toList()));

                }

            }
        }

        System.out.println();
        return new DwgResult(lineMap);
    }

    private void safeAdd(Map<Layer, List<Line>> lineMap, Layer layer, List<Line> incomingLines) {
        List<Line> lines = lineMap.getOrDefault(layer, new ArrayList<>());
        lines.addAll(incomingLines);
        lineMap.put(layer, lines);
    }

    private Map<String, Layer> layers(List<Map<String, ?>> objects) {
        return objects
                .stream()
                .filter(o -> "LAYER".equals(o.get("object")))
                .map(Layer::new)
                .collect(toMap(Layer::getHandle, Function.identity()));
    }

    private Optional<Block> toBlock(Map<String, ?> header, Map<String, Map<String, ?>> entities) {

        String name = (String) header.get("name");

        List<Entity> blockEntities = ((List<Object>) header.get("entities"))
                .stream()
                .map(Handle::toHandle)
                .filter(h -> entities.containsKey(h))
                .map(entities::get)
                .filter(Entity::isSupported)
                .map(Entity::fromMap)
                .collect(Collectors.toList());


        //  if (null == header.get("inserts")) {
        return Optional.empty();
        //}

//        List<Insertion> insertions = ((List<Object>) header.get("inserts")).stream().map(Handle::toHandle)
//                .filter(h -> entities.containsKey(h))
//                .map(entities::get)
//                .map(Insertion::fromMap)
//                .toList();
//
//        return Optional.of(new Block(name, insertions, blockEntities));
    }


}


