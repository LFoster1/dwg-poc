package dwg.test.model;


public interface Line {

    Line relativeTo(Point point);

    record Straight(Point start, Point end) implements Line {

        @Override
        public Line relativeTo(Point point) {
            return new Straight(start.add(point), end.add(point));
        }

    }

    record Circle(Point centre, double radius) implements Line {

        @Override
        public Line relativeTo(Point point) {
            return new Circle(point.add(point), radius);
        }

    }
}




