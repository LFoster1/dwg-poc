package dwg.test.model;

import java.util.List;

public record Block(String name, List<Insertion> insertions, List<Entity> entities) {
}
