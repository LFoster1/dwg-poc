package dwg.test.model;

import java.util.Map;

public class Layer {
    private final String name;
    private final String rgb;
    private final String handle;

    public Layer(Map<String, ?> layer) {
        name = (String) layer.get("name");
        Map<String, ?> color = (Map<String, ?>) layer.get("color");
        rgb = (String) color.get("rgb");

        handle = Handle.toHandle(layer.get("handle"));
    }

    public String getName() {
        return name;
    }

    public String getRgb() {
        return rgb;
    }

    public String getHandle() {
        return handle;
    }

    @Override
    public String toString() {
        return "Layer{" +
                "name='" + name + '\'' +
                ", rgb='" + rgb + '\'' +
                ", handle='" + handle + '\'' +
                '}';
    }
}
 