package dwg.test.model;

import dwg.test.JsonTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static dwg.test.JsonTest.toPoint;
import static java.util.Arrays.asList;

public abstract class Entity {

    protected final Map<String, ?> content;

    protected Entity(Map<String, ?> content) {
        this.content = content;
    }

    private static final Map<String, Function<Map<String, ?>, Entity>> SUPPORTED_TYPES =
            Map.of(
                    "LINE", LineEntity::new,
                    "LWPOLYLINE", LwPolyLineEntity::new,
                    "TEXT", Text::new,
                    "MTEXT", MText::new,
                    "CIRCLE", Circle::new
//                    "HATCH", Hatch::new
            );


    static Entity fromMap(Map<String, ?> map) {
        return SUPPORTED_TYPES.get(map.get("entity")).apply(map);
    }

    static boolean isSupported(Map<String, ?> map) {
        Object entity = map.get("entity");
        return entity != null && SUPPORTED_TYPES.containsKey(entity);
    }

    abstract List<Line> asLines();

    String getLayerHandle() {
        return Handle.toHandle(content.get("layer"));
    }

    @Override
    public String toString() {
        return "Entity{" +
                "content=" + content +
                '}';
    }

    static class LineEntity extends Entity {

        protected LineEntity(Map<String, ?> content) {
            super(content);
        }

        @Override
        public List<Line> asLines() {
            return Collections.singletonList(new Line.Straight(toPoint(content.get("start")), toPoint(content.get("end"))));
        }
    }

    static class LwPolyLineEntity extends Entity {

        protected LwPolyLineEntity(Map<String, ?> content) {
            super(content);
        }

        @Override
        public List<Line> asLines() {
            List<List<Double>> points = (List<List<Double>>) content.get("points");
            List<Point> polyPoints = points.stream().map(JsonTest::toPoint).collect(Collectors.toList());

            List<dwg.test.model.Line> lines = new ArrayList<>();
            for (int i = 0; i < polyPoints.size(); i++) {

                if (i + 1 < polyPoints.size()) {
                    lines.add(new Line.Straight(polyPoints.get(i), polyPoints.get(i + 1)));
                }
            }
            return lines;
        }
    }

    static class MText extends Entity {

        public MText(Map<String, ?> content) {
            super(content);
        }

        @Override
        List<Line> asLines() {
            Double width = (Double) content.get("rect_width");
            Double height = (Double) content.get("rect_height");

            if (null == width || width == 0) {
                width = (Double) content.get("extents_width");
            }
            if (null == height || height == 0) {
                height = (Double) content.get("extents_height");
            }

            List<Double> insertionPoint = (List<Double>) content.get("ins_pt");

            return rectangle(width, height, insertionPoint.get(0), insertionPoint.get(1));

        }
    }

    static class Text extends Entity {

        public Text(Map<String, ?> content) {
            super(content);
        }

        @Override
        List<Line> asLines() {
            double height = (double) content.get("height");
            Double widthFactor = (Double) content.get("width_factor");

            if (null == widthFactor) {
                return Collections.emptyList();
            }

            double width = height * widthFactor;
            List<Double> insertionPoint = (List<Double>) content.get("ins_pt");

            return rectangle(width, height, insertionPoint.get(0), insertionPoint.get(1));
        }
    }

    private static List<Line> rectangle(double width, double height, double startX, double startY) {
        Point topLeft = new Point(startX, startY);
        Point topRight = new Point(startX + width, startY);
        Point bottomRight = new Point(startX + width, startY - height);
        Point bottomLeft = new Point(startX, startY - height);


        return asList(
                new Line.Straight(bottomLeft, topLeft),
                new Line.Straight(topLeft, topRight),
                new Line.Straight(topRight, bottomRight),
                new Line.Straight(bottomRight, bottomLeft)
        );
    }

    static class Circle extends Entity {


        protected Circle(Map<String, ?> content) {
            super(content);
        }

        @Override
        List<Line> asLines() {
            double radius = (double) content.get("radius");
            List<Double> centerPoint = (List<Double>) content.get("center");

            Point center = new Point(centerPoint.get(0), centerPoint.get(1));

            return Collections.singletonList(new Line.Circle(center, radius));
        }
    }

    static class Hatch extends Entity {

        public Hatch(Map<String, ?> content) {
            super(content);
        }

        @Override
        List<Line> asLines() {

            List<Map<String, ?>> paths = (List<Map<String, ?>>) content.get("paths");

            List<Map<String, ?>> polyLinePaths = (List<Map<String, ?>>) paths.get(0).get("polyline_paths");

            if (null == polyLinePaths) {
                return Collections.emptyList();
            }

            List<Point> polyPoints = polyLinePaths
                    .stream().map(m -> {

                        List<Double> point = (List<Double>) m.get("point");

                        return new Point(point.get(0), point.get(1));

                    })
                    .collect(Collectors.toList());

            List<dwg.test.model.Line> lines = new ArrayList<>();
            for (int i = 0; i < polyPoints.size(); i++) {

                if (i + 1 < polyPoints.size()) {
                    lines.add(new Line.Straight(polyPoints.get(i), polyPoints.get(i + 1)));
                }
            }
            return lines;
        }
    }

}
