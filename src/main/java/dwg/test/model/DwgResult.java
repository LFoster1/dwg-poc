package dwg.test.model;

import java.util.List;
import java.util.Map;

public record DwgResult(Map<Layer, List<Line>> lines) {


}
