package dwg.test.model;

public record Point(double x, double y) {

    Point add(Point point) {
        return new Point(x + point.x * 10, y + point.y * 10);
    }

    Point multiply(Point point) {
        return new Point(x * point.x, y * point.y);
    }
}
