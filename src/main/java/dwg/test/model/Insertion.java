package dwg.test.model;

import java.util.List;
import java.util.Map;

public record Insertion(Point point, Point scale, Double rotation) {
    public static Insertion fromMap(Map<String, ?> map) {
        List<Double> point = (List<Double>) map.get("ins_pt");
        List<Double> scale = (List<Double>) map.get("scale");
        Double rotation = (Double) map.get("rotation");
        return new Insertion(
                new Point(point.get(0), point.get(1)),
                new Point(scale.get(0), scale.get(1)),
                rotation

        );
    }

}
