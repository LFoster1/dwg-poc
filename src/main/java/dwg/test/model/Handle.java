package dwg.test.model;

import java.util.List;

public class Handle {

    public static String toHandle(Object raw) {

        List<Integer> asList = ((List<Integer>) (raw));

        if (asList == null) {
            return "invalid-handle";
        }

        return String.valueOf(asList.get(asList.size() - 1));
    }

}
