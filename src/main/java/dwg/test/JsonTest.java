package dwg.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import dwg.test.model.Line;
import dwg.test.model.Point;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

public class JsonTest {


    public static void main(String[] args) throws Exception {
        List<Line> lines = getLines();

        // lines.forEach(System.out::println);
    }

    public static List<Line> getLines() {

        try {
            ObjectMapper mapper = new ObjectMapper();

            List<Map<String, ?>> objects = (List<Map<String, ?>>) mapper.readValue(new File("/Users/lewisfoster/dev/DWGtest/lew/output.json"), Map.class).get("OBJECTS");
            List<Map<String, ?>> lines = (List<Map<String, ?>>) objects.stream()
                    .filter(o -> asList("LWPOLYLINE", "LINE", "POINT").contains(o.get("entity")))
                    .collect(Collectors.toList());

            System.out.println("distinct entity start");
            objects.stream()
                    .map(o -> o.get("entity"))
                    .distinct()
                    .forEach(System.out::println);
            System.out.println("distinct entity end");

            List<Line> lineValues = lines.stream()
                    .map(
                            JsonTest::toLine
                    )
                    .flatMap(Collection::stream)
                    .collect(Collectors.toList());

            return lineValues;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static List<Line> toLine(Map<String, ?> raw) {
        String type = (String) raw.get("entity");

        switch (type) {
            case "LINE":
                return Collections.singletonList(new Line.Straight(toPoint(raw.get("start")), toPoint(raw.get("end"))));
            case "LWPOLYLINE":
                List<List<Double>> points = (List<List<Double>>) raw.get("points");
                List<Point> polyPoints = points.stream().map(JsonTest::toPoint).collect(Collectors.toList());

                List<Line> lines = new ArrayList<>();
                for (int i = 0; i < polyPoints.size(); i++) {

                    if (i + 1 < polyPoints.size()) {
                        lines.add(new Line.Straight(polyPoints.get(i), polyPoints.get(i + 1)));
                    }
                }
                return lines;

            case "POINT":
                System.out.println("Returning point");
                return Collections.singletonList(new Line.Straight(toPoint(asList(raw.get("x"), raw.get("y"))), toPoint(asList(((double) raw.get("x")) + 10, raw.get("y")))));

            default:
                throw new IllegalArgumentException("Unexpected type " + type);
        }
    }

    public static Point toPoint(Object point) {
        List<Double> pointArray = (List) point;
        return new Point(pointArray.get(0), pointArray.get(1));
    }


}
