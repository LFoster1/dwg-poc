package dwg.test;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DWG {

    @JsonRawValue
    public String json;

    @JsonProperty("OBJECTS")
    public List<DwgObject> objects;
}

@JsonIgnoreProperties(ignoreUnknown = true)
class DwgObject {

    @JsonRawValue
    public String json;

    @JsonProperty("ENTITY")
    public String entity;

    private final Map<String, String> properties;

    public DwgObject() {
        properties = new HashMap<>();
    }


    @JsonAnyGetter
    public Map<String, String> getProperties() {
        return properties;
    }

}