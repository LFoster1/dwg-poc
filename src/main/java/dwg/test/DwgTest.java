package dwg.test;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

public class DwgTest {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    public static void main(String[] args) throws Exception {


        Runtime rt = Runtime.getRuntime();
        Process ps = rt.exec("dwgread --format JSON mh.dwg");
        ps.waitFor(8, TimeUnit.SECONDS);

        InputStream output = ps.getInputStream();


        String outputString = readToString(output);

        DWG dwg = MAPPER.readValue(outputString, DWG.class);

        Files.writeString(Paths.get("/lew/mh.json"), outputString);

        System.out.println("json " + dwg.json);
        dwg.objects.stream().map(o -> o.json).distinct().forEach(o -> System.out.println(o));

        int exitValue = ps.exitValue();
        System.out.println("exit value " + exitValue);
    }

    private static String readToString(InputStream inputStream) throws IOException {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        for (int length; (length = inputStream.read(buffer)) != -1; ) {
            result.write(buffer, 0, length);
        }
        return result.toString(StandardCharsets.UTF_8);
    }


}
