package dwg.test;

import dwg.test.model.DwgResult;
import dwg.test.model.Layer;
import dwg.test.model.Line;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import static dwg.test.DrawLines.LAYER_CHECK_BOXES;
import static dwg.test.LineComponent.layerCheckbox;

public class DrawLines {

    static Map<Layer, JCheckBox> LAYER_CHECK_BOXES = new HashMap<>();

    public static void drawLines(DwgResult dwgResult) {


        JButton selectAllLayers = new JButton("Select All");
        selectAllLayers.addActionListener(e -> LAYER_CHECK_BOXES.values().forEach(c -> c.setSelected(true)));

        JButton unselectAll = new JButton("Unselect All");
        unselectAll.addActionListener(e -> LAYER_CHECK_BOXES.values().forEach(c -> c.setSelected(false)));

        Runnable r = () -> {

            JPanel contentPane = new JPanel();
            LineComponent lineComponent = new LineComponent(2000, 2000, dwgResult.lines());
            contentPane.add(lineComponent);
            LAYER_CHECK_BOXES = layerCheckbox(dwgResult.lines().keySet(), contentPane);
            contentPane.add(selectAllLayers);
            contentPane.add(unselectAll);

            JOptionPane.showMessageDialog(null, contentPane);

        };
        SwingUtilities.invokeLater(r);
    }
}

class LineComponent extends JComponent implements MouseListener, MouseWheelListener, MouseMotionListener {

    private final Map<Layer, List<Line>> lines;

    LineComponent(int width, int height, Map<Layer, List<Line>> lines) {
        super();
        setPreferredSize(new Dimension(width, height));
        this.lines = lines;
        addMouseWheelListener(this);
        addMouseMotionListener(this);


    }


    //TODO automate these based on the contents and automate controls
    //DERBY
    int xIncrease = -2800;
    int yIncrease = -1300;
    int translation = 12;

    //RUSTINGON
    {
//        xIncrease = 300;
//        yIncrease = -1000;
//        translation = 15;
    }

    //MH
//    {
//        xIncrease = 300;
//        yIncrease = -1000;
//        translation = 20;
//
//    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.white);
        g.fillRect(0, 0, getWidth(), getHeight());
        Dimension d = getPreferredSize();
        g.setColor(Color.black);

        lines.forEach((layer, lines) -> {

            lines.stream()
                    .filter(Line.Straight.class::isInstance)
                    .map(Line.Straight.class::cast)
                    .forEach(line -> drawLine(g, layer, line));

            lines.stream()
                    .filter(Line.Circle.class::isInstance)
                    .map(Line.Circle.class::cast)
                    .forEach(line -> drawLine(g, layer, line));


        });
    }

    private void drawLine(Graphics g, Layer layer, Line.Circle lineToDraw) {
        Graphics2D g2d = (Graphics2D) g;

        double x1 = lineToDraw.centre().x();
        double y1 = lineToDraw.centre().y();
        double radius = lineToDraw.radius();

        x1 = x1 / translation;
        y1 = y1 / translation;

        //radius = radius / translation;

        x1 += xIncrease;
        y1 += yIncrease;

        g.setColor(Color.red);

        Ellipse2D.Double circle = new Ellipse2D.Double(x1 - (radius), y1 - (radius), (radius * 2) + xIncrease, (radius * 2) + yIncrease);


        if (LAYER_CHECK_BOXES.get(layer).isSelected()) {
            g2d.draw(circle);
        }


    }

    private void drawLine(Graphics g, Layer layer, Line.Straight lineToDraw) {

        double x1 = lineToDraw.start().x();
        double y1 = lineToDraw.start().y();
        double x2 = lineToDraw.end().x();
        double y2 = lineToDraw.end().y();

        x1 = x1 / translation;
        x2 = x2 / translation;
        y1 = y1 / translation;
        y2 = y2 / translation;

        x1 += xIncrease;
        x2 += xIncrease;
        y1 += yIncrease;
        y2 += yIncrease;

        Line2D.Double line = new Line2D.Double(
                x1,
                -y1,
                x2,
                -y2
        );

        g.setColor(Color.black);
        if (LAYER_CHECK_BOXES.get(layer).isSelected()) {
            g.drawLine(
                    (int) line.getX1(),
                    (int) line.getY1(),
                    (int) line.getX2(),
                    (int) line.getY2()
            );
        }

    }

    static Map<Layer, JCheckBox> layerCheckbox(Set<Layer> layers, JComponent parent) {

        JPanel contentPane = new JPanel();
        JPanel listOfLayers = new JPanel();
        listOfLayers.setLayout(new BoxLayout(listOfLayers, BoxLayout.Y_AXIS));


        Map<Layer, JCheckBox> checkBoxes = layers.stream()
                .collect(Collectors.toMap(Function.identity(), l -> {
                    JCheckBox checkbox = new JCheckBox(l.getName());
                    checkbox.addItemListener(e -> parent.repaint());
                    checkbox.setSelected(true);
                    return checkbox;
                }));

        Map<Layer, JCheckBox> sorted = new TreeMap<>(Comparator.comparing(Layer::getName));
        sorted.putAll(checkBoxes);
        checkBoxes = Collections.synchronizedMap(sorted);

        checkBoxes.values().forEach(listOfLayers::add);

        contentPane.add(new JLabel("Layers"));

        JScrollPane jScrollPane = new JScrollPane(listOfLayers);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        jScrollPane.getVerticalScrollBar().setUnitIncrement(16);
        jScrollPane.setPreferredSize(new Dimension(400, 400));
        contentPane.add(jScrollPane);
        parent.add(contentPane);


        return checkBoxes;

    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {

//        if (e.getWheelRotation() > 0) {
//            if (translation < 16) {
//                translation += 1;
//            }
//        } else {
//            if (translation > 1) {
//                translation -= 4;
//            }
//        }
//        repaint();
    }

    Point mousePressed = null;

    @Override
    public void mouseDragged(MouseEvent e) {
//        if (mousePressed != null) {
//            int dx = e.getX() - mousePressed.x;
//            int dy = e.getY() - mousePressed.y;
//
//            xIncrease += (dx / 10);
//            yIncrease += (dy / 10);
//            mousePressed = e.getPoint();
//            repaint();
//        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        mousePressed = e.getPoint();
    }

    @Override
    public void mousePressed(MouseEvent e) {
        mousePressed = e.getPoint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
